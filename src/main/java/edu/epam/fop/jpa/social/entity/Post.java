package edu.epam.fop.jpa.social.entity;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a social media post.
 */
@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Integer id;

    @Column(name = "content")
    private String content;

    @Column(name = "author")
    private String author;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    /**
     * Adds a comment to this post.
     *
     * @param comment The comment to add.
     */
    public void addComment(Comment comment) {
        comments.add(comment);
        comment.setPost(this);
    }

    /**
     * Removes a comment from this post.
     *
     * @param comment The comment to remove.
     */
    public void removeComment(Comment comment) {
        comments.remove(comment);
        comment.setPost(null);
    }

    /**
     * Gets the ID of the post.
     *
     * @return The post ID.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the ID of the post.
     *
     * @param id The post ID.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the content of the post.
     *
     * @return The post content.
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the content of the post.
     *
     * @param content The post content.
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Gets the author of the post.
     *
     * @return The post author.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets the author of the post.
     *
     * @param author The post author.
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Gets the list of comments on this post.
     *
     * @return The list of comments.
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * Sets the list of comments on this post.
     *
     * @param comments The list of comments.
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
