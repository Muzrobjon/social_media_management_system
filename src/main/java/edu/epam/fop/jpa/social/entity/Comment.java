package edu.epam.fop.jpa.social.entity;

import jakarta.persistence.*;

/**
 * Represents a comment on a social media post.
 */
@Entity
@Table(name = "comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @Column(name = "comment_text")
    private String commentText;

    @Column(name = "commenter")
    private String commenter;

    /**
     * Gets the ID of the comment.
     *
     * @return The comment ID.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the ID of the comment.
     *
     * @param id The comment ID.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the post associated with this comment.
     *
     * @return The associated post.
     */
    public Post getPost() {
        return post;
    }

    /**
     * Sets the post associated with this comment.
     *
     * @param post The associated post.
     */
    public void setPost(Post post) {
        this.post = post;
    }

    /**
     * Gets the text content of the comment.
     *
     * @return The comment text.
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * Sets the text content of the comment.
     *
     * @param commentText The comment text.
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * Gets the commenter's name.
     *
     * @return The commenter's name.
     */
    public String getCommenter() {
        return commenter;
    }

    /**
     * Sets the commenter's name.
     *
     * @param commenter The commenter's name.
     */
    public void setCommenter(String commenter) {
        this.commenter = commenter;
    }
}
