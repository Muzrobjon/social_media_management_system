package edu.epam.fop.jpa.social;

import edu.epam.fop.jpa.social.service.SocialMediaService;
import edu.epam.fop.jpa.social.service.SocialMediaServiceImpl;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
/**
 * The `Main` class serves as the entry point for the Social Media application.
 * It initializes the `EntityManagerFactory`, creates posts, adds comments,
 * and demonstrates various social media interactions.
 *
 * @author Muzrabjon Tolipov
 * @version 1.0
 * @since 2024-03-28
 */
public class Main {
    /**
     * The main method initializes the application by creating an `EntityManagerFactory`,
     * setting up the `SocialMediaService`, and demonstrating post and comment operations.
     *
     * @param args Command-line arguments (not used in this application).
     */
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("SocialMediaPU");
        EntityManager em = entityManagerFactory.createEntityManager();
        SocialMediaService socialMediaService = new SocialMediaServiceImpl(em);

        // Creating a post
        Integer postId = socialMediaService.createPost(post -> {
            post.setContent("Content 1");
            post.setAuthor("Author 1");
        });

        // Adding comments to the post
        socialMediaService.addComment(postId, comment -> {
            comment.setCommenter("Commenter 11");
            comment.setCommentText("Comment 11");
        });

        Integer comment12 = socialMediaService.addComment(postId, comment -> {
            comment.setCommenter("Commenter 12");
            comment.setCommentText("Comment 12");
        });

        // Creating a post
        Integer postId2 = socialMediaService.createPost(post -> {
            post.setContent("Content 2");
            post.setAuthor("Author 2");
        });

        // Adding comments to the post
        socialMediaService.addComment(postId2, comment -> {
            comment.setCommenter("Commenter 21");
            comment.setCommentText("Comment 21");
        });

        socialMediaService.addComment(postId2, comment -> {
            comment.setCommenter("Commenter 22");
            comment.setCommentText("Comment 22");
        });

        // Deleting one comment
        // Assuming commentId is known
        socialMediaService.removeComment(comment12);

        // Deleting the post along with its comments
        socialMediaService.removePost(postId2);
        System.out.println("exit");
    }
}
