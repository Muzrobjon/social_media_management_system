package edu.epam.fop.jpa.social.service;

import edu.epam.fop.jpa.social.entity.Comment;
import edu.epam.fop.jpa.social.entity.Post;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

import java.util.function.Consumer;
/**
 * The `SocialMediaServiceImpl` class provides implementation for the `SocialMediaService` interface.
 * It manages posts, comments, and interactions within the social media application.
 *
 * @author Muzrabjon Tolipov
 * @version 1.0
 * @since 2024-03-28
 */
public class SocialMediaServiceImpl implements SocialMediaService {

    private EntityManager entityManager;

    /**
     * Constructs a `SocialMediaServiceImpl` with the specified `EntityManager`.
     *
     * @param entityManager The `EntityManager` used for database interactions.
     */
    public SocialMediaServiceImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Creates a new post with the provided configuration.
     *
     * @param postConfig A consumer that configures the post (setting content, author, etc.).
     * @return The ID of the newly created post.
     */
    @Override
    public Integer createPost(Consumer<Post> postConfig) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        Post post = new Post();
        postConfig.accept(post);
        entityManager.persist(post);

        transaction.commit();

        return post.getId();
    }

    /**
     * Adds a comment to an existing post.
     *
     * @param postId       The ID of the post to which the comment will be added.
     * @param commentConfig A consumer that configures the comment (setting commenter, text, etc.).
     * @return The ID of the newly created comment.
     * @throws IllegalArgumentException If the specified post ID does not exist.
     */
    @Override
    public Integer addComment(Integer postId, Consumer<Comment> commentConfig) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        Post post = entityManager.find(Post.class, postId);
        if (post == null) {
            throw new IllegalArgumentException("Post with ID " + postId + " not found.");
        }

        Comment comment = new Comment();
        commentConfig.accept(comment);
        post.addComment(comment);

        entityManager.persist(comment);

        transaction.commit();

        return comment.getId();
    }

    /**
     * Removes a post along with its associated comments.
     *
     * @param postId The ID of the post to be removed.
     */
    @Override
    public void removePost(Integer postId) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        Post post = entityManager.find(Post.class, postId);
        if (post != null) {
            entityManager.remove(post);
        }

        transaction.commit();
    }

    /**
     * Removes a comment.
     *
     * @param commentId The ID of the comment to be removed.
     */
    @Override
    public void removeComment(Integer commentId) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        // Implementation details for comment removal...

        transaction.commit();
    }
}
