package edu.epam.fop.jpa.social.service;

import edu.epam.fop.jpa.social.entity.Comment;
import edu.epam.fop.jpa.social.entity.Post;

import java.util.function.Consumer;
/**
 * The `SocialMediaService` interface defines methods for managing posts and comments
 * within the social media application.
 *
 * @author Muzrabjon Tolipov
 * @version 1.0
 * @since 2024-03-28
 */
public interface SocialMediaService {

    /**
     * Creates a new post with the provided configuration.
     *
     * @param postConfig A consumer that configures the post (setting content, author, etc.).
     * @return The ID of the newly created post.
     */
    Integer createPost(Consumer<Post> postConfig);

    /**
     * Adds a comment to an existing post.
     *
     * @param postId       The ID of the post to which the comment will be added.
     * @param commentConfig A consumer that configures the comment (setting commenter, text, etc.).
     * @return The ID of the newly created comment.
     * @throws IllegalArgumentException If the specified post ID does not exist.
     */
    Integer addComment(Integer postId, Consumer<Comment> commentConfig);

    /**
     * Removes a post along with its associated comments.
     *
     * @param postId The ID of the post to be removed.
     */
    void removePost(Integer postId);

    /**
     * Removes a comment.
     *
     * @param commentId The ID of the comment to be removed.
     */
    void removeComment(Integer commentId);
}
